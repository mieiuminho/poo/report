# Introdução {#sec:intro}

Desenvolvemos durante o semestre uma aplicação semelhante à sobejamente
conhecida aplicação 'Airbnb', no entanto, a nossa aplicação destina-se ao
aluguer de transportes, com o intuito de transportar o cliente do ponto 'X'
para o 'Y' no intervalo de tempo mais curto possível.

A arquitetura da aplicação assenta em 3 estruturas essenciais: o cliente, o
proprietário (de transportes) e o próprio transporte. Dos referidos
anteriormente os atores do sistema são: os clientes e os proprietários, na
medida em que são as decisões tomadas por estes que dinamizam a estrutura de
dados da aplicação (o estado).

O relatório está organizado do seguinte modo. Na próxima secção será resumido
muito brevemente o enunciado e os objetivos que nos propomos a cumprir neste
trabalho. Na @sec:classes são explicadas as estruturas usadas para manter os
dados e as razões que levaram à sua escolha de cada uma das estruturas. Por
fim, comentários conclusivos são apresentados na @sec:end.
