# Breve descrição do enunciado {#sec:enunciado}

Conforme foi já mencionado na introdução, o objetivo deste projeto é
implementar, em Java, uma aplicação que assenta em pressupostos semelhantes aos
da aplicação 'Airbnb' só que ao invés de se alugarem sítios para permanecer,
alugam-se transportes para viagens específicas.

O sistema que implementa a aplicação tens dois atores principais: os clientes e
os donos.

Existe um conjunto de ações que a aplicação tem de implementar obrigatório:

Para os clientes:

* solicitar o aluguer do carro mais próximo das suas coordenadas;
* solicitar o carro mais barato;
* solicitar o aluguer do carro mais barato dentro de uma distância que estão
dispostos a percorrer a pé;
* solicitar o aluguer de um carro específico;
* solicitar o aluguer de um carro com uma autonomia específica;

Para os proprietários:

* sinalizar que um dos seus carros até disponível para aluguer;
* abastecer o veículo;
* alterar o preço por quilómetro;
* aceitar/rejeitar o aluguer de um determinado cliente;
* registar quanto custou a viagem;

A aplicação implementa vários tipos de veículos: carros a gasolina, a gasóleo,
elétricos, híbridos e tem de estar preparada para que possam ser introduzidos
outros tipos de veículos no futuro, como trotinetes elétricas e bicicletas, por
exemplo.

Por forma a tentar assegurar o máximo de fidelidade possível para com a
realidade o cálculo dos tempos aproximados de viagem tem em conta a hora do dia
(para fazer uma previsão do estado do trânsito e os atrasados por isso
causados) assim como o estado meteorológico da localização atual do cliente.
