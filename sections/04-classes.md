# Classes {#sec:classes}

## User

``` java
private String name;
private int nif;
private String email;
private String adress;
private String hashedPassword;
private LocalDateTime birth;
private double rating;
private List<Aluguer> rents;
private List<Notification> pendingTasks;
private List<Double> classificacoes;
```

A presente classe é abstrata e inclui as variáveis de instância comuns aos
objetos do tipo `Owner` e do tipo `Client`.

Procede-se agora à explicação das variáveis de instância e perceber a razão da
sua necessidade assim como o seu significado no contexto da aplicação:

* `name` String que contém o nome do usuário.
* `nif` Inteiro que contém o Número de Identificação Fiscal do usuário.
* `email` String que contém o email do usuário.
* `adress` String que contém a morada do usuário.
* `hashedPassword` String que contém a password do usuário. No entanto, não
é guardada diretamente a password de cada usuário: é calculada uma hash de 256
bits da password e guardado esse resultado.
* `birth` LocalDateTime que guarda o momento em que foi criada a conta.
* `rating` Double que contém o rating do usuário (média das classificações que
lhe foram atribuídas até ao momento).
* `rents` Lista de estruturas do tipo `Aluguer` que sintetizam os alugueres
realizados pelo usuário até ao momento.
* `pendingTasks` Lista de tarefas pendentes do usuário. No caso de ser um
`Owner` pode ser aceitar/rejeitar um pedido de aluguer ou avaliar um cliente,
caso seja um `Client` pode ser avaliar proprietário/carro.
* `classificacoes` Lista de classificações atribuídas ao usuário até ao momento
presente.

## Client

```java
private Point2D.Double position;
```

Em virtude de estender a classe `User` esta classe conta com todas as variáveis
de instância abordadas no item anterior, com adição de `position` que
representa a posição atual do cliente.

## Owner

Esta classe estende `Owner` pelo que herda todas as variáveis de instância.

## Transport

```java
private String id;
private int nifDono;
private String email;
private Point2D.Double position;
private double autonomy;
private double capacity;
private double avgVelocity;
private double rating;
private double priceKm;
private List<Aluguer> alugueres;
private List<Double> classificacoes;
private LocalDateTime availableAt;
```

A presente classe é abstrata e inclui variáveis de instância que serão comuns a
todos os tipos de transportes que possam vir a fazer parte da aplicação.
Vamos agora perceber o significado das variáveis de instância no contexto da
aplicação:

* `id` String que contém a matrícula ou forma de identificação do transporte.
* `nifDono` Inteiro que contém o Número de Identificação Fiscal do proprietário
do veículo.
* `email` String que contém o email do proprietário do transporte.
* `position` Posição atual do transporte.
* `autonomy` Autonomia do transporte.
* `capacity` Capacidade do depósito do transporte.
* `avgVelocity` Velocidade média do transporte.
* `rating` Avaliação média do transporte.
* `priceKm` Preço do transporte por quilómetro.
* `alugueres` Lista de alugueres em que o transporte foi utilizado.
* `classificacoes` Lista de classificações atribuídas ao transporte.
* `availableAt` LocalDateTime que contém a hora a que o transporte ficará livre,
caso esteja ocupado de momento.

## Car

```java
private String marca;
private double consumoPercentage;
```
Esta classe estende `Transport` pelo que herda todas as variáveis de instância
cujo significado foi explicado no item anterior. Adicionamos `marca` que é uma
String que contém a marca do carro e `consumoPercentage` que é o consumo do
carro por quilómetro.  A presente classe serve para modelar carros elétricos, a
gasolina ou a gasóleo uma vez que embora os combustíveis sejam diferentes a
maneira de os modular é completamente análoga, visto que apendas precisamos de
guardar a marca do carro e o seu consumo.

## Hybrid

```java
private String marca;
private consumoGas;
private consumoEletrico;
private autonomiaGas;
private autonomiaEletrico;
```
À semelhança da classe anterior esta classe estende `Transport` pelo que herda
as variáveis de instância da mesma. Adicionamos `marca` que é uma String que
contém a marca do carro.  `conumosGas` e `consumoEletrico` refletem o consumo
dos motores a combustíveis fosseis e elétrico, respetivamente, enquanto que
`autonomiaGas` e `autonomiaEletrico` quantificam a autonomia dos motores a
combustíveis fosseis e elétrico, respetivamente.

## UMCarroJa

Esta é a classe que agrupa os itens anteriores.

```java
private Map<String, Owner> owners;
private Map<String, Client> clients;
private Map<String, Transport> transports;
```
O estado que mantemos para guardar os dados da aplicação é uma instância desta
classe. Esta é a classe onde se encontram também implementadas a funcionalidade
de gravar o estado atual da aplicação e o restauro (`load`) de um estado prévio
da aplicação.

## Controlador

Esta classe é responsável por reger a interação entre a informação pedida ao
utilizador e o seu posterior tratamento de modo a exercer ações sobre o estado
e manipulá-lo.  É responsável por comunicar com o modelo e com a visualização,
ou seja, manipula todo o fluxo da aplicação.
