# Introdução de novos transportes {#sec:transportes}

A aplicação foi desenvolvida tendo em mente a introdução de novos tipos de
transportes e por isso mesmo a classe `Transport` é abstrata.  É de notar que a
implementação do método `moveTransport(Point2D.Double origin, Point2D.Double
destination)` é delegada às classes que estendem a classe abstrata. Este método
é responsável também por atualizar a autonomia do transporte, verificar se o
transporte tem autonomia suficiente e se está disponível e por isso mesmo a
introdução de um novo transporte reside essencialmente na criação da classe com
as eventuais nuances e implementação do método acima referido.  No caso dos
transportes referidos no enunciado: Bicicletas, Trotinetes, (...) como estes
não têm um combustível basta que no método `moveTransport(Client c,
Point2D.Double destination)` seja ignorada a atualização da autonomia e a
confirmação de que tem combustível suficiente, bastando apenas verificar se no
momento da requisição do aluguer o transporte se encontra ocupado ou não.
