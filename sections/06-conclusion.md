# Conclusão {#sec:end}

Os requisitos definidos para a aplicação `UMCarroJá!` foram cumpridos na sua
totalidade. O produto final é uma aplicação de terminal que, caso o utilizador
pretenda permite recuperar os dados da anterior execução da aplicação, assim
como permite ao cliente e aos proprietários realizar as ações que tinham sido
enumeradas no enunciado como mandatórias.

Este projeto seguiu princípios de Engenharia de Software, tais como,
encapsulamento de dados, modularidade e reutilização de código. Tais princípios
foram expostos durante a unidade curricular e consolidados durante o
desenvolvimento deste programa.

A modularidade desta aplicação permitiria, com relativa facilidade, a
introdução de novas funcionalidades que a tornariam mais dinâmica. Estas
passariam por, por exemplo, adicionar diferentes tipos de veículos.

O sucesso deste projeto é o resultado da aquisição de conhecimentos na presente
unidade curricular aliados ao modelo de programação Model - View - Controller.
