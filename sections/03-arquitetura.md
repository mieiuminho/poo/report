# Arquitetura {#sec:arquitetura}

A aplicação foi desenvolvida tendo sempre em mente o modelo de desenvolvimento
MVC (Model - View - Controller).

Assim, o programa tem três pilares basilares: o *Model* que agrupa todas as
classes que implementam a parte algorítmica da aplicação, ou seja, estruturas
de dados, operações sobre as referidas estruturas; a *View* que codifica todas
as operações de interação de com o utilizador, quer apresentação de resultados,
quer recolha de inputs necessários ao funcionamento do programa; o *Controller*
assume também um papel preponderante na medida em que é este que providencia um
canal de comunicação entre a *View* e o *Model*. É o *Controller* que controla
todo o fluxo do programa.
